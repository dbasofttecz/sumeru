<?php
/**
 * Created by PhpStorm.
 * User: woocodstudio
 * Date: 5/8/2017
 * Time: 7:34 PM
 */
get_header(); ?>


<div class="four-zero-pages-design">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>404 pages</h1>
                <p>Page not found</p>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
