<?php show_admin_bar( false );
add_filter( 'acf/settings/show_admin', '__return_false' );
function remove_menus(){
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'plugins.php' );                //Plugins
}
add_action( 'admin_menu', 'remove_menus' );


function wp_script_share() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_style( 'bootstrap-woocod-woocod', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'bootstrap-woocod-css-bootstrap', get_template_directory_uri() . '/css/booty.min.css' );
	
	wp_enqueue_style( 'dfuck-animated', get_template_directory_uri() . '/css/animate.min.css' );
	wp_enqueue_style( 'fontawesoe-woocod-woocod', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'slick-theme-woocod', get_template_directory_uri() . '/css/slick-theme.css' );
	wp_enqueue_style( 'slick-woocod-woocod', get_template_directory_uri() . '/css/slick.css' );
	wp_enqueue_style( 'slick-google-raleway-woocod', 'https://fonts.googleapis.com/css?family=Raleway:400,600,700' );
	wp_enqueue_style( 'slick-google-Oswald-woocod', 'https://fonts.googleapis.com/css?family=Oswald' );
	wp_enqueue_style( 'chat-google-Oswald-woocod', 'https://fonts.googleapis.com/css?family=Poppins:400,600,700' );
	wp_enqueue_style( 'chat-google-encryptio', 'http://weloveiconfonts.com/api/?family=entypo' );
	wp_enqueue_style( 'core-woocod-woocod', get_template_directory_uri() . '/css/core.css' );
	wp_enqueue_style( 'core-woocod-responsive', get_template_directory_uri() . '/css/responsive.css' );
}

add_action( 'wp_enqueue_scripts', 'wp_script_share' );

function wp_footer_script() {
	wp_enqueue_script( 'jQuery-woocod', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js' );
	wp_enqueue_script( 'bootstrap-woocod-jquery', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( 'booty-js', get_template_directory_uri() . '/js/booty.js' );
	wp_enqueue_script( 'bootstrap-woocod-slick', get_template_directory_uri() . '/js/slick.min.js' );
	wp_enqueue_script( 'bootstrap-woocod-core', get_template_directory_uri() . '/js/core.js' );
}

add_action( 'wp_footer', 'wp_footer_script' );
add_theme_support( 'post-thumbnails' );


register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'woocodstudio' ),
) );

/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string See optional args description above.
 *
 * @return object|WP_Error the registered post type object, or an error object
 */
function announcement_group() {
	
	$labels = array(
		'name'               => __( 'Announcement', 'text-domain' ),
		'singular_name'      => __( 'Announcement', 'text-domain' ),
		'add_new'            => _x( 'Add New Announcement', 'text-domain', 'text-domain' ),
		'add_new_item'       => __( 'Add New Announcement', 'text-domain' ),
		'edit_item'          => __( 'Edit Announcement', 'text-domain' ),
		'new_item'           => __( 'New Announcement', 'text-domain' ),
		'view_item'          => __( 'View Announcement', 'text-domain' ),
		'search_items'       => __( 'Search Announcement', 'text-domain' ),
		'not_found'          => __( 'No Announcement found', 'text-domain' ),
		'not_found_in_trash' => __( 'No Announcement found in Trash', 'text-domain' ),
		'parent_item_colon'  => __( 'Parent Announcement:', 'text-domain' ),
		'menu_name'          => __( 'Announcement', 'text-domain' ),
	);
	
	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-megaphone',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title',
			'editor',
			'thumbnail'
		)
	);
	
	register_post_type( 'announcement', $args );
}

add_action( 'init', 'announcement_group' );

require_once get_template_directory() . '/core-function/woocod-customizer.php';
require_once get_template_directory() . '/core-function/walker.php';


add_action( 'init', 'codex_book_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_book_init() {
	$labels = array(
		'name'               => _x( 'Invesement', 'post type general name', 'www.woocod.com' ),
		'singular_name'      => _x( 'Book', 'post type singular name', 'www.woocod.com' ),
		'menu_name'          => _x( 'Invesement', 'admin menu', 'www.woocod.com' ),
		'name_admin_bar'     => _x( 'Book', 'add new on admin bar', 'www.woocod.com' ),
		'add_new'            => _x( 'Add New', 'book', 'www.woocod.com' ),
		'add_new_item'       => __( 'Add New Book', 'www.woocod.com' ),
		'new_item'           => __( 'New Book', 'www.woocod.com' ),
		'edit_item'          => __( 'Edit Book', 'www.woocod.com' ),
		'view_item'          => __( 'View Book', 'www.woocod.com' ),
		'all_items'          => __( 'All Invesement', 'www.woocod.com' ),
		'search_items'       => __( 'Search Invesement', 'www.woocod.com' ),
		'parent_item_colon'  => __( 'Parent Invesement:', 'www.woocod.com' ),
		'not_found'          => __( 'No Invesement found.', 'www.woocod.com' ),
		'not_found_in_trash' => __( 'No Invesement found in Trash.', 'www.woocod.com' )
	);
	
	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'www.woocod.com' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'menu_icon'          => 'dashicons-money',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'investment' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
	);
	
	register_post_type( 'investment', $args );
}

/* register taxonomy */
add_action( 'init', 'inv_tax_onomy' );
function inv_tax_onomy() {
	register_taxonomy(
		'invtax',
		'investment',
		array(
			'label'        => __( 'Investment Tax' ),
			'rewrite'      => array( 'slug' => 'inv_tax' ),
			'hierarchical' => true,
		)
	);
}


if ( ! function_exists( 'twentyfourteen_post_nav' ) ) :
	/**
	 * Display navigation to next/previous post when applicable.
	 *
	 * @since Twenty Fourteen 1.0
	 */
	function twentyfourteen_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );
		if ( ! $next && ! $previous ) {
			return;
		}
		?>
        <nav class="navigation post-navigation" role="navigation">
            <div class="nav-links">
				<?php
				if ( is_attachment() ) :
					previous_post_link( '%link', __( '<span class="meta-nav">Published In</span>%title', 'twentyfourteen' ) );
				else :
					previous_post_link( '%link', __( '<span class="meta-nav">Previous Post</span>%title', 'twentyfourteen' ) );
					next_post_link( '%link', __( '<span class="meta-nav">Next Post</span>%title', 'twentyfourteen' ) );
				endif;
				?>
            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
		<?php
	}
endif;
/*
 * Option page for advertisement section
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	$option_page = acf_add_options_page( array(
		'page_title' => 'Advertisement Section',
		'menu_title' => 'Advertisement Section',
		'menu_slug'  => 'advertisement-sections',
		'capability' => 'edit_posts',
		'redirect'   => false,
		'icon_url'   => 'dashicons-megaphone',
		'position'   => 1
	) );	

	$option_page = acf_add_options_page( array(
		'page_title' => 'Trade Area',
		'menu_title' => 'Trade Area',
		'menu_slug'  => 'trade-area',
		'capability' => 'edit_posts',
		'redirect'   => false,
		'icon_url'   => 'dashicons-chart-line',
		'position'   => 1
	) );
	
}
/*
* Advertisement contact page
*/
function advertisement_right_hand_side() {
	
	$contact_title_header = get_field( 'advertisement_title', 'option' );
	
	// start date and ending date //
	$advertisement_start_date_contact = get_field( "advertisement_start_date", "option" );
	$advertisement_end_date_contact   = get_field( "advertisement_end_date", "option" );
	
	//images and link
	$image_footer_header       = get_field( "advertisement_images", "option" );
	$image_url_contact         = $image_footer_header['url'];
	$image_contact_header_link = get_field( "advertisement_url", "option" );
    $image_link_contact = "";
	if ( $image_contact_header_link ) {
		$image_link_contact = "#";
	}
	
	$start_new_date = new DateTime( $advertisement_start_date_contact );
	$end_new_date   = new DateTime( $advertisement_end_date_contact );
	// date start variable
	$start_new_date_comp_contact = strtotime( $start_new_date->format( 'j M Y' ) );
	$end_new_date_comp_contact   = strtotime( $end_new_date->format( 'j M Y' ) );
	//today date
	$today      = date( "j M Y" );
	$today_date = strtotime( $today );
	//do comparism
	if ( ! empty( $start_new_date_comp_contact ) || ! empty( $end_new_date_comp_contact ) ) {
		if ( $start_new_date_comp_contact <= $today_date && $end_new_date_comp_contact >= $today_date ) {
			if ( ! empty( $image_url_contact ) ) {
				echo '<a href="' . esc_url( $image_link_contact ) . '" target="_blank" alt="' . $contact_title_header . '">';
				echo '<img src="' . $image_url_contact . ' " class="img-responsive" >';
				echo '</a>';
			}
			
		} elseif ( $start_new_date_comp_contact >= $today_date ) {
			echo "<div class='coming'>Advertise Coming Soon !</div>";
		} else {
			echo "<div class='expiry'>Advertise Expired !</div>";
		}
	}
	
}

/*
 * mid section
 */
function advertisement_mid_hand_side() {
	
	$contact_title_header_mid = get_field( 'advertisement_title_mid', 'option' );
	
	// start date and ending date //
	$advertisement_start_date_contact_mid = get_field( "advertisement_start_date_mid", "option" );
	$advertisement_end_date_contact_mid   = get_field( "advertisement_end_date_mid", "option" );
	
	//images and link
	$image_footer_header_mid       = get_field( "advertisement_images_mid", "option" );
	$image_url_contact_mid         = $image_footer_header_mid['url'];
	$image_contact_header_link_mid = get_field( "advertisement_url_mid", "option" );
	
	if ( $image_contact_header_link_mid ) {
		$image_link_contact_mid = "#";
	}
	
	$start_new_date = new DateTime( $advertisement_start_date_contact_mid );
	$end_new_date   = new DateTime( $advertisement_end_date_contact_mid );
	// date start variable
	$start_mid_date_middle_section = strtotime( $start_new_date->format( 'j M Y' ) );
	$end_middle_section            = strtotime( $end_new_date->format( 'j M Y' ) );
	//today date
	$today      = date( "j M Y" );
	$today_date = strtotime( $today );
	//do comparism
	if ( ! empty( $start_mid_date_middle_section ) || ! empty( $end_middle_section ) ) {
		if ( $start_mid_date_middle_section <= $today_date && $end_middle_section >= $today_date ) {
			if ( ! empty( $image_url_contact_mid ) ) {
				echo '<a href="' . esc_url( $image_url_contact_mid ) . '" target="_blank" alt="' . $contact_title_header_mid . '">';
				echo '<img src="' . $image_url_contact_mid . ' " class="img-responsive" >';
				echo '</a>';
			}
			
		} elseif ( $start_mid_date_middle_section >= $today_date ) {
			echo "<div class='coming'>Advertise Coming Soon !</div>";
		} else {
			echo "<div class='expiry'>Advertise Expired !</div>";
		}
	}
	
}

/*
 * social media share buttons
 */
if ( ! function_exists( 'resolution_share' ) ) :
	function resolution_share() {
		global $post;
		// var_dump($post);
		echo '

<li>
<a target="_blank" href="http://twitter.com/home?status=';
		the_title();
		echo ' - ';
		echo home_url() . "/" . $post->post_name;
		echo '"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 112.197 112.197" style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve">

<circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
<path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/></path></svg></a>
</li>



<li>
<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=';
// https://www.facebook.com/sharer/sharer.php?u=tejsoln.com/careers/&t=PHP%20DEVELOPER
		the_permalink();
		echo '&amp;t=';
		the_title();
		echo '"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
<circle style="fill:#3B5998;" cx="56.098" cy="56.098" r="56.098"/>
<path style="fill:#FFFFFF;" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/></path></svg></a>
</li>


						<li>
			<a target="_blank" href="http://www.google.com/bookmarks/mark?op=edit&amp;bkmk=';
		the_permalink();
		echo '&amp;title=';
		the_title();
		echo '"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
		<circle id="XMLID_30_" style="fill:#DC4E41;" cx="56.098" cy="56.097" r="56.098"/>
		<path style="fill:#DC4E41;" d="M19.531,58.608c-0.199,9.652,6.449,18.863,15.594,21.867c8.614,2.894,19.205,0.729,24.937-6.648
			c4.185-5.169,5.136-12.06,4.683-18.498c-7.377-0.066-14.754-0.044-22.12-0.033c-0.012,2.628,0,5.246,0.011,7.874
			c4.417,0.122,8.835,0.066,13.252,0.155c-1.115,3.821-3.655,7.377-7.51,8.757c-7.443,3.28-16.94-1.005-19.282-8.813
			c-2.827-7.477,1.801-16.5,9.442-18.675c4.738-1.667,9.619,0.21,13.673,2.673c2.054-1.922,3.976-3.976,5.864-6.052
			c-4.606-3.854-10.525-6.217-16.61-5.698C29.526,35.659,19.078,46.681,19.531,58.608z"/></path>
		<path style="fill:#DC4E41;" d="M79.102,48.668c-0.022,2.198-0.045,4.407-0.056,6.604c-2.209,0.022-4.406,0.033-6.604,0.044
			c0,2.198,0,4.384,0,6.582c2.198,0.011,4.407,0.022,6.604,0.045c0.022,2.198,0.022,4.395,0.044,6.604c2.187,0,4.385-0.011,6.582,0
			c0.012-2.209,0.022-4.406,0.045-6.615c2.197-0.011,4.406-0.022,6.604-0.033c0-2.198,0-4.384,0-6.582
			c-2.197-0.011-4.406-0.022-6.604-0.044c-0.012-2.198-0.033-4.407-0.045-6.604C83.475,48.668,81.288,48.668,79.102,48.668z"/></path>

			<path style="fill:#FFFFFF;" d="M19.531,58.608c-0.453-11.927,9.994-22.949,21.933-23.092c6.085-0.519,12.005,1.844,16.61,5.698
				c-1.889,2.077-3.811,4.13-5.864,6.052c-4.054-2.463-8.935-4.34-13.673-2.673c-7.642,2.176-12.27,11.199-9.442,18.675
				c2.342,7.808,11.839,12.093,19.282,8.813c3.854-1.38,6.395-4.936,7.51-8.757c-4.417-0.088-8.835-0.033-13.252-0.155
				c-0.011-2.628-0.022-5.246-0.011-7.874c7.366-0.011,14.743-0.033,22.12,0.033c0.453,6.439-0.497,13.33-4.683,18.498
				c-5.732,7.377-16.322,9.542-24.937,6.648C25.981,77.471,19.332,68.26,19.531,58.608z"/></path>
			<path style="fill:#FFFFFF;" d="M79.102,48.668c2.187,0,4.373,0,6.57,0c0.012,2.198,0.033,4.407,0.045,6.604
				c2.197,0.022,4.406,0.033,6.604,0.044c0,2.198,0,4.384,0,6.582c-2.197,0.011-4.406,0.022-6.604,0.033
				c-0.022,2.209-0.033,4.406-0.045,6.615c-2.197-0.011-4.396,0-6.582,0c-0.021-2.209-0.021-4.406-0.044-6.604
				c-2.197-0.023-4.406-0.033-6.604-0.045c0-2.198,0-4.384,0-6.582c2.198-0.011,4.396-0.022,6.604-0.044
				C79.057,53.075,79.079,50.866,79.102,48.668z"/></path>

</svg></a>
			</li>



<li>
			<a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=';
		the_permalink();
		echo '&amp;title=';
		the_title();
		echo '"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
	    <circle style="fill:#007AB9;" cx="56.098" cy="56.097" r="56.098"/>
		<path style="fill:#F1F2F2;" d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
			c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
			c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
			C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
			c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
			 M27.865,83.739H41.27V43.409H27.865V83.739z"/></path></svg></a>
			</li>';
	}

endif; // ======================= RESOLUTION SOCIAL SHARE BUTTONS ======================== //

/*** pagination ***/

function wp_bootstrap_pagination( $args = array() ) {
    
    $defaults = array(
        'range'           => 4,
        'custom_query'    => FALSE,
        'previous_string' => __( 'Previous', 'text-domain' ),
        'next_string'     => __( 'Next', 'text-domain' ),
        'before_output'   => '<div class="post-nav"><ul class="pager">',
        'after_output'    => '</ul></div>'
    );
    
    $args = wp_parse_args( 
        $args, 
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );
    
    $args['range'] = (int) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = (int) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );
    
    if ( $count <= 1 )
        return FALSE;
    
    if ( !$page )
        $page = 1;
    
    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ($count - $ceil) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }
    
    $echo = '';
    $previous = intval($page) - 1;
    $previous = esc_attr( get_pagenum_link($previous) );
    
    $firstpage = esc_attr( get_pagenum_link(1) );
    if ( $firstpage && (1 != $page) )
        $echo .= '<li class="previous"><a href="' . $firstpage . '">' . __( 'First', 'text-domain' ) . '</a></li>';

    if ( $previous && (1 != $page) )
        $echo .= '<li><a href="' . $previous . '" title="' . __( 'previous', 'text-domain') . '">' . $args['previous_string'] . '</a></li>';
    
    if ( !empty($min) && !empty($max) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ($page == $i) {
                $echo .= '<li class="active"><span class="active">' . str_pad( (int)$i, 2, '0', STR_PAD_LEFT ) . '</span></li>';
            } else {
                $echo .= sprintf( '<li><a href="%s">%002d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
            }
        }
    }
    
    $next = intval($page) + 1;
    $next = esc_attr( get_pagenum_link($next) );
    if ($next && ($count != $page) )
        $echo .= '<li><a href="' . $next . '" title="' . __( 'next', 'text-domain') . '">' . $args['next_string'] . '</a></li>';
    
    $lastpage = esc_attr( get_pagenum_link($count) );
    if ( $lastpage ) {
        $echo .= '<li class="next"><a href="' . $lastpage . '">' . __( 'Last', 'text-domain' ) . '</a></li>';
    }

    if ( isset($echo) )
        echo $args['before_output'] . $echo . $args['after_output'];
}
/***** breadcumb ****/
function qt_custom_breadcrumbs() {
 
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '&raquo;'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  global $post;
  $homeLink = get_bloginfo('url');
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '<div id="crumbs">You are here: <a href="' . $homeLink . '">' . $home . '</a></div>';
 
  } else {
 
    echo '<div id="crumbs">You are here: <a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
} // end qt_custom_breadcrumbs()

/////////////////////////////
/////////Register Post type//
/////////////////////////////

/////////////////////////////
/////////Register Post type//
/////////////////////////////
