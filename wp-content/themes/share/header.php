<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo('name')?> | <?php bloginfo('description'); ?></title>
	<?php wp_head(); ?>
</head>
<body>
<div class="nav-bav ">
<div class="example3">
  <nav class="navbar navbar-default navbar-static-top "  >
      <div class="overlay"></div>
    <div class="container">
    <div class="row">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo site_url('/' ); ?>">
<?php $logo_wocood = get_theme_mod('logo_image');
if($logo_wocood) {  ?>
    <img src="<?php echo $logo_wocood; ?>" alt="Test ">
<?php } ?>
        </a>
      </div>
      <div id="navbar3" class="navbar-collapse collapse">
        <?php wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker())
            ); ?>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
    </div>
    <!--/.container-fluid -->
  </nav>
<div class="container">
<div class="ticker-container">
  <div class="ticker-caption">
    <p>Breaking News</p>
  </div>
  <ul>
  <?php 
  $arg_slider = array(
  'post_type' => 'post', 
  'posts_per_page' => 3,
  'order_by' => 'date',
  'order' => 'DESC'
  );
    $the_query = new WP_Query($arg_slider);
    while($the_query->have_posts() ) : $the_query->the_post();  ?>
    <div>
      <li><span><?php the_title(); ?> &ndash; <a href="<?php the_permalink();  ?>"><?php _e("read more"); ?></a></span></li>
    </div>
  <?php endwhile; ?>
  </ul>
</div>
</div>

</div>

 




</div>