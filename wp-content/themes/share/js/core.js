jQuery(document).ready(function () {
    jQuery('.slider-aero').slick({
        autoplay: true,
        arrows: false,
        dots: false,
        fade: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        // adaptiveHeight: true
    });


    // jQuery(".hero__video-link").click(function () {
    //     jQuery('.hero--slider').slick('slickPause');
    // });
});

////////////////// News ticker woocod studio ////////
var speed = 5000;
canTick = true;

jQuery(document).ready(function ($) {
    $('.ticker-container ul div').each(function (i) {
        if ($(window).width() >= 500) {
            $(this).find('li').width($(window).width() - parseInt($(this).css('left')));
        }
        if (i == 0) {
            $(this).addClass('ticker-active');
        } else {
            $(this).addClass('not-active');
        }
        if ($(this).find('li').height() > 30) {
            $(this).find('li').css({
                'height': '20px',
                'width': '200%',
                'text-align': 'left',
                'padding-left': '5px'
            });
            $(this).find('li').css('width', $(this).find('li span').width());
        }
    });
    startTicker();
    animateTickerElementHorz();
});

$(window).resize(function () {
    $('.ticker-container ul div').each(function (i) {
        if ($(this).find('li').height() > 30) {
            $(this).css({
                'height': '20px',
                'width': '200%',
                'text-align': 'left',
                'padding-left': '5px'
            });
            $(this).find('li').css('width', $(this).find('li span').width());
        }
    });
});

function startTicker() {
    setInterval(function () {
        if (canTick) {
            $('.ticker-container ul div.ticker-active')
                .removeClass('ticker-active')
                .addClass('remove');
            if ($('.ticker-container ul div.remove').next().length) {
                $('.ticker-container ul div.remove')
                    .next()
                    .addClass('next');
            } else {
                $('.ticker-container ul div')
                    .first()
                    .addClass('next');
            }
            $('.ticker-container ul div.next')
                .removeClass('not-active next')
                .addClass('ticker-active');
            setTimeout(function () {
                $('.ticker-container ul div.remove')
                    .css('transition', '0s ease-in-out')
                    .removeClass('remove')
                    .addClass('not-active finished');
                if ($(window).width() < 500) {
                    if ($('.ticker-container ul div.finished li').width() < $(window).width()) {
                        $('.ticker-container ul div.finished').removeClass('finished');
                    }
                } else {
                    if ($('.ticker-container ul div.finished li').width() < ($(window).width() - (parseInt($('.ticker-container ul div.ticker-active').css('left')) + 15))) {
                        $('.ticker-container ul div.finished').removeClass('finished');
                    }
                }
                setTimeout(function () {
                    $('.ticker-container ul div')
                        .css('transition', '0.25s ease-in-out');
                }, 75);
                animateTickerElementHorz();
            }, 250);
        }
    }, speed);
}

function animateTickerElementHorz() {
    if ($(window).width() < 500) {
        if ($('.ticker-container ul div.ticker-active li').width() > $(window).width()) {
            setTimeout(function () {
                $('.ticker-container ul div.ticker-active li').animate({
                    'margin-left': '-' + (($('.ticker-container ul div.ticker-active li').width() - $(window).width()) + 15)
                }, speed - (speed / 5), 'swing', function () {
                    setTimeout(function () {
                        $('.ticker-container ul div.finished').removeClass('finished').find('li').css('margin-left', 0);
                    }, ((speed / 5) / 2));
                });
            }, ((speed / 5) / 2));
        }
    } else {
        if ($('.ticker-container ul div.ticker-active li').width() > ($(window).width() - parseInt($('.ticker-container ul div.ticker-active').css('left')))) {
            setTimeout(function () {
                $('.ticker-container ul div.ticker-active li').animate({
                    'margin-left': Math.abs($('.ticker-container ul div.ticker-active li').width() - ($(window).width() - parseInt($('.ticker-container ul div.ticker-active').css('left'))) + 15) * -1
                }, speed - (speed / 5), 'swing', function () {
                    setTimeout(function () {
                        $('.ticker-container ul div.finished').removeClass('finished').find('li').css('margin-left', 0);
                    }, ((speed / 5) / 2));
                });
            }, ((speed / 5) / 2));
        }
    }
}

$('.ticker-container').on('mouseover', function () {
    canTick = false;
});

$('.ticker-container').on('mouseout', function () {
    canTick = true;
});


jQuery(document).ready(function () {
    // global vars
    // var winWidth = jQuery(window).width();
    var winHeight = jQuery(window).height();

// set initial div height / width
    jQuery('.full-height-banner .slick-track').css({
        'height': winHeight - 200,
    });

    jQuery('.img-res a img').css({
        'height': winHeight - 200,
    });

// make sure div stays full width/height on resize
    jQuery(window).resize(function () {
        jQuery('.full-height-banner .slick-track').css({
            'height': winHeight,
        });
    });
    jQuery(window).resize(function () {
        jQuery('.img-res a img').css({
            'height': winHeight,
        });
    });


});

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



jQuery('.slider-slick-wrapper-sumeru').slick({
    infinite: true,
    autoplay: true,
    arrows: false,
    dots: false,
    fade: false,
    slidesToShow: 3,
    slidesToScroll: 1,
});