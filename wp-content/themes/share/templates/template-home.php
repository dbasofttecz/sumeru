<?php
/*
* 
* Template Name: Home 
*/
get_header(); ?>
<div class="container-fluid">
    <div class="row hidden-md hidden-sm hidden-phone hidden-xs">
        <div class="wrapper-content-sumeru">
            <div class="col-md-5 padder">
                <div class="row">
                 <?php 
                    if( have_rows('options_repeaters', 'option') ): 
                    while ( have_rows('options_repeaters', 'option') ) : the_row(); 
                    $title_trade = get_sub_field('point_change', 'option');
                    $symbol_trade = get_sub_field('symbol_nepse', 'option');
                    $currentindextrade = get_sub_field('current_index', 'option');
                    $percentchange = get_sub_field('percenter_change', 'option');
                    $colorpicker = get_sub_field('color_selector', 'option');
                    $selector_case_wise = get_sub_field('slider_section_or_not', 'option');
                    if($selector_case_wise == 'disableslide') {
                ?>
                       <div class="col-lg-6 padder ">
                                        <div class="banner-box red coli">
                                            <b style="background-color: <?php echo $colorpicker; ?>"><?php echo $symbol_trade; ?></b>
                                            <ul>
                                                <li><strong><?php echo $currentindextrade; ?></strong><span>Current Index</span></li>
                                                <li><strong><?php echo $title_trade; ?></strong><span>Point Change</span></li>
                                                <li><strong><?php  echo $percentchange; ?></strong><span>% Change</span></li>
                                            </ul>
                                        </div>
                                    </div>
                   
                <?php } endwhile; else: endif;?>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="slider-slick-wrapper-sumeru">


                                 <?php 
                    if( have_rows('options_repeaters', 'option') ): 
                    while ( have_rows('options_repeaters', 'option') ) : the_row(); 
                    $title_trade = get_sub_field('point_change', 'option');
                    $symbol_trade = get_sub_field('symbol_nepse', 'option');
                    $currentindextrade = get_sub_field('current_index', 'option');
                    $percentchange = get_sub_field('percenter_change', 'option');
                    $colorpicker = get_sub_field('color_selector', 'option');
                    $selector_case_wise = get_sub_field('slider_section_or_not', 'option');
                    if($selector_case_wise == 'slide') {
                ?>
                    <div class="banner-box green">
                        <b style="background-color: <?php echo $colorpicker; ?>"><?php echo $symbol_trade; ?></b>
                        <ul>
                           <li><strong><?php echo $currentindextrade; ?></strong><span>Current Index</span></li>
                                                <li><strong><?php echo $title_trade; ?></strong><span>Point Change</span></li>
                                                <li><strong><?php  echo $percentchange; ?></strong><span>% Change</span></li>
                        </ul>
                    </div>
         <?php } endwhile; else: endif;?>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid extra">
    <div class="row">
        <div class="full-height-banner col-md-7 extra-space">

    <div class="slider-aero">
     <?php
                $arg_slider = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'order_by' => 'date',
                    'order' => 'DESC'
                );
                $the_query = new WP_Query($arg_slider);
                while ($the_query->have_posts()) : $the_query->the_post();
                    $image_slider = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                    if ($image_slider) {
                        $slider_placeholder = $image_slider[0];
                    } else {
                        $slider_placeholder = get_template_directory_uri() . "/img/slider-placeholder.jpg";
                    }
                    ?>
        <div class="content-slide">
            <img src="<?php echo $slider_placeholder; ?>" alt="<?php the_title(); ?>">
            <div class="content-wrapper-text">
                <h1><?php the_title(); ?></h1>
                <a href="<?php the_permalink(); ?>" class="btn btn-default custom-btn">read more</a>
            </div>
        </div>

               <?php endwhile; ?>
         </div>
    </div>

       <div class="full-height-banner col-md-5 extra-space">
            <div class="img-res">
                <?php advertisement_right_hand_side(); ?>
                <!--                <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/adv.jpg" >-->
            </div>
        </div>
</div>

<?php while (have_posts()) : the_post(); ?>
    <div class="welcome-message">
        <div class="container">
            <div class="row">
             <h1><?php the_title(); ?></h1>
                <div class="col-md-11 text-center col-centered">
                    <div class="wel-com-form">
                        <div class="content-wrapper"><p><?php echo wp_trim_words(get_the_content(), 560, ""); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>

</div>

<div class="all-list-item">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php _e("Latest News"); ?></h1>

                <?php $array_post_annouincement_dipsllay = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3
                );
                $query_loop = new wp_query($array_post_annouincement_dipsllay);
                if ($query_loop->have_posts()) : while ($query_loop->have_posts()) : $query_loop->the_post();
                    $image_slider = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                    if ($image_slider) {
                        $placeholder = $image_slider[0];
                    } else {
                        $placeholder = get_template_directory_uri() . "/img/placeholder.png";
                    }
                    ?>
                    <div class="media">
                        <div class="media-left">
                            <a href="<?php the_permalink(); ?>">
                                <img class="media-object" src="<?php echo $placeholder; ?>" alt="...">
                            </a>

                            <div class="media-body">
                                <h4 class="media-heading"><a
                                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <p> <?php echo wp_trim_words(get_the_content(), 16, '...'); ?>
                                </p>
                            </div>

                        </div>
                    </div>
                <?php endwhile; endif;
                wp_reset_postdata(); ?>
            </div>
            <div class="col-md-6">
                <div class="media">
                    <h1><?php _e("Announcement"); ?></h1>
                    <ul>
                        <?php $array_post_annouincement_dipsllay = array(
                            'post_type' => 'announcement',
                            'posts_per_page' => 10
                        );
                        $query_loop = new wp_query($array_post_annouincement_dipsllay);
                        if ($query_loop->have_posts()) : while ($query_loop->have_posts()) : $query_loop->the_post();
                            ?>
                            <a href="<?php the_permalink(); ?>">
                                <li>
                                    <?php echo wp_trim_words(get_the_title(), 4, "..."); ?>
                                    <time><?php echo the_time('d M Y'); ?>
                                        / <?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' ago'; ?></time>
                                </li>
                            </a>
                        <?php endwhile; endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="advertisment-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="div-image-section">
                    <!--                    <img src="-->
                    <?php //echo get_template_directory_uri(); ?><!--/img/adver.png"-->
                    <!--                                                    class="img-responsive">-->
                    <?php advertisement_mid_hand_side(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="latest-news-section">
    <div class="container">
        <div class="row">
            <h1>News</h1>
            <?php $array_post_annouincement_dipsllay = array(
                'post_type' => 'post',
                'posts_per_page' => 12,
                'order_by' => 'date',
                'order' => 'DESC'
            );
            $query_loop = new wp_query($array_post_annouincement_dipsllay);
            if ($query_loop->have_posts()) : while ($query_loop->have_posts()) : $query_loop->the_post();
                $image_slider = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                if ($image_slider) {
                    $placeholder = $image_slider[0];
                } else {
                    $placeholder = get_template_directory_uri() . "/img/placeholder.png";
                }
                ?>
                <div class="col-md-3">
                    <div class="img-sec" style="background-image: url(<?php echo $placeholder; ?>)"></div>
                    <time class="todaydate"><?php the_time("M d Y"); ?></time>
                    <h2><a href="<?php the_permalink() ?>"> <?php echo wp_trim_words(get_the_title(), 10, ""); ?></a>
                    </h2>
                </div>
            <?php endwhile; endif; ?>
        </div>
        <div class="text-center">
            <a href="<?php echo home_url('/blog/'); ?>" class="btn btn-default btn-lg custom-viewall">View all</a>
        </div>
    </div>
</div>

<div class="investment-tabeed">
    <div id="exTab1" class="container">
        <h1>Investment</h1>
        <ul class="nav nav-pills">
            <?php $trerms = get_terms('invtax', array());
            foreach ($trerms as $term_taxonomy) { ?>
                <li class="">
                    <a href="#<?php echo $term_taxonomy->slug; ?>"
                       data-toggle="tab"><?php echo $term_taxonomy->name; ?></a>
                </li>

            <?php } ?>
        </ul>
        <div class="tab-content clearfix">


            <?php
            $trerms = get_terms('invtax', array());
            foreach ($trerms as $term_taxonomy) {
                $arg = array(
                    'post_type' => 'investment',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'invtax',
                            'field' => 'slug',
                            'terms' => $term_taxonomy->slug,
                        ),
                    ),
                );
                $the_loop_query = new WP_Query($arg);
                while ($the_loop_query->have_posts()) : $the_loop_query->the_post(); ?>
                    <div class="tab-pane" id="<?php echo $term_taxonomy->slug; ?>">
                        <table class="table table-hover">
                            <thead>
                            <th><?php _e("S.N"); ?></th>
                            <?php if (get_field("display_symbol_no_or_not") == "enable") { ?>
                                <th><?php _e("Symbol"); ?></th><?php } ?>
                            <th><?php _e("Company name"); ?></th>
                            <?php if (get_field("display_ratio_not") == "enable") { ?> 
                            <th><?php _e("Ratio"); ?></th><?php } ?>
                            <th><?php _e("Units"); ?></th>
                            <th><?php _e("Opening date"); ?></th>
                            <th><?php _e("Closing date"); ?></th>
                            <th><?php _e("Status"); ?></th>
                            <th><?php _e("View"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (have_rows('investment_row')): ?>
                                <?php while (have_rows('investment_row')): the_row();
                                    $snno = get_sub_field("sn");
                                    $snno_symbol = get_sub_field("symbol_no");
                                    $snno_ratio = get_sub_field("ratio");
                                    $snno_inits = get_sub_field("units");
                                    $snno_opening = get_sub_field("opening_date");
                                    $snno_closing = get_sub_field("closing_date");
                                    $snno_status = get_sub_field("status"); 
                                    $link_page = get_sub_field("view_link"); 

                                    ?>
                                    <tr>
                                        <?php if (!empty($snno)) { ?>
                                            <td><?php echo $snno; ?></td><?php } ?>
                                        <?php if (get_field("display_symbol_no_or_not") == "enable") { ?>
                                            <td><?php echo $snno_symbol; ?></td><?php } ?>
                                        <td><?php the_title(); ?></td>
                                        <?php if (get_field("display_ratio_not") == "enable") { ?>
                                            <td><?php echo $snno_ratio; ?></td><?php } ?>
                                        <?php if (!empty($snno_inits)) { ?>
                                            <td><?php echo $snno_inits; ?></td><?php } ?>
                                        <?php if (!empty($snno_opening)) { ?>
                                            <td><?php echo $snno_opening; ?></td><?php } ?>
                                        <?php if (!empty($snno_closing)) { ?>
                                            <td><?php echo $snno_closing; ?></td><?php } ?>
                                        <?php if (!empty($snno_status)) { ?>
                                            <td><?php echo $snno_status; ?></td><?php } ?>
                                             <?php if (!empty($link_page)) { ?>
                                        <td><a href="<?php echo esc_url($link_page); ?>">
                                                <i class="fa fa-eye" aria-hidden="true"></i>View</a></td>
                                                <?php } else { ?>
                                               <td><a>
                                                <i class="fa fa-eye" aria-hidden="true"></i>Link not set</a></td>
                                                <?php } ?>
                                    </tr>
                                <?php endwhile; ?>
                            <?php endif; //if( get_sub_field('items') ): ?>
                            </tbody>
                        </table>
                    </div>
                <?php endwhile;
            } ?>

        </div>
    </div>
</div>
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(".investment-tabeed .container .nav li:first").addClass("active");
    jQuery(".investment-tabeed .tab-content .tab-pane:first").addClass("active");
</script>