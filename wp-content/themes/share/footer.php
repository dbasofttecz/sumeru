<div class="call-to-action-share">
    <div class="container">
        <div class="row">
			<?php $call_to_action = get_theme_mod( "calltoaction" );
			if ( ! empty( $call_to_action ) ) { ?>
                <div class="col-md-9">
                    <h1><?php echo $call_to_action; ?></h1>
                </div>
			<?php }
			$page_calltoaction = get_theme_mod( 'page_link_section' );
			if ( ! empty( $page_calltoaction ) ) { ?>
                <div class="col-md-3">
                    <a href="<?php echo get_permalink( $page_calltoaction ); ?>"
                       class="btn btn-default btn-calltoaction">Contact
                        us</a>
                </div>
			<?php } ?>
        </div>
    </div>
</div>

<div class="footer">
    <div class="overlay"></div>
    <div class="container">
		<?php $abt_page = get_theme_mod( "page_link_open" );
		$args           = array( 'post_type' => 'page', 'p' => $abt_page );
		$the_query_abt  = new wp_query( $args );
		if ( $the_query_abt->have_posts() ) : while ( $the_query_abt->have_posts() ) : $the_query_abt->the_post();
			?>
            <div class="col-md-3 col-sm-3">
                <div class="about-share">
                    <h1><?php the_title(); ?></h1>
                    <p><?php echo wp_trim_words( get_the_content(), 50, ".." ); ?></p>

                </div>
            </div>
		<?php endwhile; endif;
		wp_reset_postdata(); ?>

        <div class="col-md-3 col-sm-3">
            <div class="useful-share">
                <h1>Recent announcement</h1>
                <ul>
					<?php
					$args         = array( 'post_type' => 'announcement', 'numberposts' => '5' );
					$recent_posts = wp_get_recent_posts( $args );
					foreach ( $recent_posts as $recent ) {
						echo '<li><a href="' . get_permalink( $recent["ID"] ) . '"> <i class="fa fa-bullhorn"
                                                                   aria-hidden="true"></i> ' . wp_trim_words( $recent["post_title"], 2, ".." ) . '</a> </li> ';
					}
					wp_reset_query(); ?>
                </ul>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="facebook-likebox">
                <div id="fb-root"></div>
            </div>


            <div class="fb-page" data-href="https://www.facebook.com/Sumeru39/" data-tabs="timeline" data-width="264"
                 data-height="280" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true"
                 data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/Sumeru39/" class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/Sumeru39/">Sumeru Securities P. Ltd</a></blockquote>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="get-share">
                <h1>Get in Touch</h1>
                <ul>
				    <?php $get_in_touch = get_theme_mod( 'address_address' );
				    if ( ! empty( $get_in_touch ) ) {
					    ;
				    }
				    { ?>
                        <li><a href=">"><i class="fa fa-location-arrow"
                                           aria-hidden="true"></i><?php echo $get_in_touch; ?></a></li><?php } ?>
				    <?php $get_in_touch = get_theme_mod( 'mobile_phone' );
				    if ( ! empty( $get_in_touch ) ) {
					    ;
				    }
				    { ?>
                        <li><a href="tel:<?php echo $get_in_touch; ?>"><i class="fa fa-volume-control-phone"
                                                                          aria-hidden="true"></i><?php echo $get_in_touch; ?>
                        </a></li><?php } ?>
				    <?php $get_in_touch = get_theme_mod( 'emailaddress' );
				    if ( ! empty( $get_in_touch ) ) {
					    ;
				    }
				    { ?>
                        <li><a href="mailto:<?php echo $get_in_touch; ?>"><i class="fa fa-envelope-o"
                                                                             aria-hidden="true"></i><?php echo $get_in_touch; ?>
                        </a></li><?php } ?>
                </ul>

            </div>
        </div>
    </div>

</div>


<div class="copyright">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-4">
                <div class="copyright-links">
                    Copyright <?php the_date( 'Y' ); ?> <?php bloginfo( 'name' ); ?>. All right reserved.

                </div>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="love-developer">
                    Made by <a target="_blank" href="<?php _e( "http://dbasofttech.com" ); ?>">&nbsp;DBASoftTech</a>&nbsp;&nbsp;with
                    <div class="heart">
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<aside id="sticky-social">
    <ul>
		<?php $floating_social_media = get_theme_mod( 'facebook_links' );
		if ( ! empty( $floating_social_media ) ) { ?>
            <li><a target="_blank" href="<?php echo $floating_social_media; ?>" class="entypo-facebook" target="_blank"><span>Facebook</span></a>
            </li><?php } ?>
		<?php $floating_social_media = get_theme_mod( 'tw_links' );
		if ( ! empty( $floating_social_media ) ) { ?>
            <li><a target="_blank" href="<?php echo $floating_social_media; ?>" class="entypo-twitter"
                   target="_blank"><span>Twitter</span></a></li><?php } ?>
		<?php $floating_social_media = get_theme_mod( 'goo_gle_plus' );
		if ( ! empty( $floating_social_media ) ) { ?>
            <li><a target="_blank" href="<?php echo $floating_social_media; ?>" class="entypo-gplus"
                   target="_blank"><span>Google+</span></a></li><?php } ?>
    </ul>
</aside>

<?php wp_footer(); ?>
</body>
</html>
