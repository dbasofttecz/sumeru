<?php
get_header();
while ( have_posts() ) : the_post();
	?>
    <div class="header-banner">
        <div class="container">
            <div class="row">
                <div class="header-content-wrapper">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="single-content-wrapper">
					<?php $image_slider = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
					if ( $image_slider ) {
						$img_reseos = $image_slider[0];
					} else {
						$img_reseos = get_template_directory_uri() . '/img/slider-placeholder.jpg';
					}
					?>
                    <div class="img-responsive-dba" style="background-image: url(<?php echo $img_reseos; ?>)"></div>
					<?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>

