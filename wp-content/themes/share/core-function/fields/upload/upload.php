<?php
function really_simple_image_widget_upload_enqueue($hook)
{
  if( $hook != 'widgets.php' )
      return;
  wp_enqueue_style('thickbox');
  wp_enqueue_script('media-upload');
    wp_enqueue_style('chat-google-jquery-uri-woocod', 'http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
  wp_enqueue_script('thickbox');
  wp_enqueue_script('upload-script', get_template_directory_uri() .'/core-function/fields/upload/upload.js' , null, null, true);
}
add_action('admin_enqueue_scripts', 'really_simple_image_widget_upload_enqueue');
