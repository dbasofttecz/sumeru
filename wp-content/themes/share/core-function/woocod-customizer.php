<?php

function woocod_customizer_register($wp_customize)
{
    // Do stuff with $wp_customize, the WP_Customize_Manager object.



    $wp_customize->add_panel('theme_option', array(
        'priority' => 40,
        'title' => __('Theme Option', 'woocod'),
        'description' => __('', 'woocod')
    ));

    /**********************************************/
    /*************** LOGO SECTION *****************/
    /**********************************************/

    $wp_customize->add_section('theme_logo', array(
        'priority' => 40,
        'title' => __('Sumeru Logo', 'woocod'),
        'description' => '',
        'panel' => 'theme_option'
    ));

    $wp_customize->add_setting('logo_image', array(
        'sanitize_callback' => 'esc_url_raw',
        'default' => ''
    ));


    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_image', array(
        'label' => __('Logo Section', 'woocod'),
        'section' => 'theme_logo',
        'settings' => 'logo_image'
    )));
/****
 * social media
 */
    $wp_customize->add_section('social_media', array(
        'priority' => 40,
        'title' => __('Social Media', 'woocod'),
        'description' => '',
        'panel' => 'theme_option'
    ));

    $wp_customize->add_setting('facebook_links', array(
        'sanitize_callback' => 'esc_url_raw',
        'default' => ''
    ));


    $wp_customize->add_control( 'facebook_links', array(
        'label' => __('Facebook Link', 'woocod'),
        'section' => 'social_media',
        'settings' => 'facebook_links'
    ));
/* tw link */
    $wp_customize->add_setting('tw_links', array(
        'sanitize_callback' => 'esc_url_raw',
        'default' => ''
    ));


    $wp_customize->add_control( 'tw_links', array(
        'label' => __('Twitter Link', 'woocod'),
        'section' => 'social_media',
        'settings' => 'tw_links'
    ));
    /* google link */
    $wp_customize->add_setting('goo_gle_plus', array(
        'sanitize_callback' => 'esc_url_raw',
        'default' => ''
    ));


    $wp_customize->add_control( 'goo_gle_plus', array(
        'label' => __('Google Link', 'woocod'),
        'section' => 'social_media',
        'settings' => 'goo_gle_plus'
    ));

    /*
     * call to action
     */

    $wp_customize->add_section('call_to_ac', array(
        'priority' => 40,
        'title' => __('Call to action', 'woocod'),
        'description' => '',
        'panel' => 'theme_option'
    ));

    $wp_customize->add_setting('calltoaction', array(
        'sanitize_callback' => '',
        'default' => ''
    ));


    $wp_customize->add_control( 'calltoaction', array(
        'label' => __('Call to action content', 'woocod'),
        'section' => 'call_to_ac',
        'settings' => 'calltoaction',
        'type' => 'textarea'
    ));


    $wp_customize->add_setting('page_link_section', array(
        'sanitize_callback' => '',
        'default' => ''
    ));


    $wp_customize->add_control( 'page_link_section', array(
        'label' => __('Call to action button link', 'woocod'),
        'section' => 'call_to_ac',
        'settings' => 'page_link_section',
        'type' => 'dropdown-pages'
    ));

    /**
     * footer about us page
     */
    $wp_customize->add_section('abt_page_link', array(
        'priority' => 40,
        'title' => __('Footer About us', 'woocod'),
        'description' => '',
        'panel' => 'theme_option'
    ));


    $wp_customize->add_setting('page_link_open', array(
        'sanitize_callback' => '',
        'default' => ''
    ));


    $wp_customize->add_control( 'page_link_open', array(
        'label' => __('Footer About us page content display', 'woocod'),
        'section' => 'abt_page_link',
        'settings' => 'page_link_open',
        'type' => 'dropdown-pages'
    ));
    /*
     * get in touch
     */

    $wp_customize->add_section('get_in_touch', array(
        'priority' => 40,
        'title' => __('Get In Touch', 'woocod'),
        'description' => '',
        'panel' => 'theme_option'
    ));

    $wp_customize->add_setting('address_address', array(
    'sanitize_callback' => '',
    'default' => ''
));


    $wp_customize->add_control( 'address_address', array(
        'label' => __('Address', 'woocod'),
        'section' => 'get_in_touch',
        'settings' => 'address_address',
        'type' => 'textarea'
    ));

    $wp_customize->add_setting('mobile_phone', array(
        'sanitize_callback' => '',
        'default' => ''
    ));


    $wp_customize->add_control( 'mobile_phone', array(
        'label' => __('Phone', 'woocod'),
        'section' => 'get_in_touch',
        'settings' => 'mobile_phone',
    ));

    $wp_customize->add_setting('emailaddress', array(
        'sanitize_callback' => '',
        'default' => ''
    ));


    $wp_customize->add_control( 'emailaddress', array(
        'label' => __('Email Address', 'woocod'),
        'section' => 'get_in_touch',
        'settings' => 'emailaddress',
    ));


}

add_action('customize_register', 'woocod_customizer_register');


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function woocod_customize_preview_js()
{
    wp_enqueue_script('woocod_customizer', get_template_directory_uri() . '/js/customizer.js', array(
        'customize-preview'
    ), '20130508', true);
}
add_action('customize_preview_init', 'woocod_customize_preview_js');


