<?php
// For Upload field
require_once get_template_directory() . "/core-function/fields/upload/upload.php";

if (!class_exists('AdvertiseWidget')) {

class AdvertiseWidget extends WP_Widget {
	
	function AdvertiseWidget(){
		global $control_ops, $post_cat, $post_num, $post_length;	

		$widget_ops = array(						
						'description' => __( 'Advertisement', 'Sajha-Khabar') 
						);
		
		$this->WP_Widget('AdvertiseWidget', __('Advertisement', 'Sajha-Khabar'), $widget_ops, $control_ops);

		
	}	
	
	function widget($args, $instance){

		extract($args);				

		$startdate 			=  isset( $instance['startdate'] ) ? $instance['startdate'] : '';
		$enddate 			=  isset( $instance['enddate'] ) ? $instance['enddate'] : '';	
		$imagesec 			=  isset( $instance['imagesec'] ) ? $instance['imagesec'] : '';
		$imgurl 			=  isset( $instance['imgurl'] ) ? esc_url($instance['imgurl']) : '#';
		$adv_iframe 		=  isset( $instance['adv_iframe'] ) ? $instance['adv_iframe'] : '';	
		if(empty($imgurl))
		{
			$imgurl='#';
		}
		
		?> 

 
<?php   
		echo $args['before_widget'];
		$today=date("Y/m/d");
        $today_date= strtotime($today);
        $adv_start=strtotime($startdate);
        $adv_end=strtotime($enddate);
	// checking advertisement date to display  
 	  if(!empty($adv_start) || !empty($adv_end))
 	  {
			  	 if($adv_start <= $today_date && $adv_end >= $today_date)
			  	 {
			  	 		if(!empty($imagesec))
			  	 		{	echo '<li><a href="'.$imgurl.'" target="_blank">';
			  	 			echo '<img src="'.$imagesec.' " >';
			  	 			echo '</a></li>';
			  	 		}
			  	 		else
			  	 		{
			  	 			 echo '<li>'.$adv_iframe.'</li>';
			  	 		}

			  	 }

		 	  	 elseif($adv_start >= $today_date )
		 	  	 {
		 	  	 	 echo 'Advertise Coming Soon !' ;	
		 	  	 }
		 	  	 else
		 	  	 {
		 	  	 	 echo 'Advertise Expired !' ;	
		 	  	 }

 	  }

   echo $args['after_widget'];

?>



			
		
	<?php
	}	
		
	function form($instance){	
		
		$instance = wp_parse_args( (array) $instance, array(
						'startdate'			=> '', 	
						'enddate'			=> '',
						'imagesec'			=> '',
						'imgurl'			=> '',	
						'adv_iframe'		=> '',				
						) 
					);

		$startdate 			=  isset( $instance['startdate'] ) ? $instance['startdate'] : '';
		$enddate 			=  isset( $instance['enddate'] ) ? $instance['enddate'] : '';	
		$imagesec 			=  isset( $instance['imagesec'] ) ? $instance['imagesec'] : '';	
		$imgurl 			=  isset( $instance['imgurl'] ) ? esc_url($instance['imgurl']) : '#';
		$adv_iframe 		=  isset( $instance['adv_iframe'] ) ? $instance['adv_iframe'] : '';
		
	  ?>
	    <script>
	  	jQuery(document).ready(function($) {
		jQuery('.custom_date').datepicker({
		dateFormat : 'yy-mm-dd'
		});

		


		});
	  </script>

	

		<p>
		<label for="<?php echo $this->get_field_name('startdate'); ?>">
		<?php _e('Advertise Starting Date', 'Sajha-Khabar'); ?>: </label>
		<input class="widefat custom_date" name="<?php echo $this->get_field_name('startdate'); ?>" type="text" value="<?php echo $startdate; ?>" placeholder="Adv. starting date"/>		
		</p>

		<p>
		<label for="<?php echo $this->get_field_name('enddate'); ?>">
		<?php _e('Advertise Ending Date', 'Sajha-Khabar'); ?>:</label>
		<input class="widefat custom_date" name="<?php echo $this->get_field_name('enddate'); ?>" type="text" value="<?php echo $enddate; ?>" placeholder="Adv. enddate date"/>		
		</p>

		<p>	
		<label for="<?php echo $this->get_field_name('imagesec'); ?>">
		<?php _e('Advertise Image'); ?>:</label><br/>
		<?php
		if(!empty($imagesec))
		{
			echo '<img src="'.$imagesec.'" alt="" style="width:50%;"/> <br/>';
		}
		?>
		<input class="img" size="30" id="<?php echo $this->get_field_id('imagesec'); ?>" name="<?php echo $this->get_field_name('imagesec'); ?>" type="text" value="<?php echo $imagesec; ?>" />
		<input class="select-img button" name="select-img button" type="button" value="<?php esc_attr_e( 'Insert Image ', 'sajha-khabar' ); ?>" />
		</p>

		<p>	
		<label for="<?php echo $this->get_field_name('imgurl'); ?>">
		<?php _e('Image Link'); ?>:</label><br/>
		<input class="widefat" size="30" id="<?php echo $this->get_field_id('imgurl'); ?>" name="<?php echo $this->get_field_name('imgurl'); ?>" type="text" value="<?php echo $imgurl; ?>" />
		</p>
		<p>	
		<label for="<?php echo $this->get_field_name('adv_iframe'); ?>">
		<?php _e('Iframe'); ?>:</label><br/>
		<textarea class="widefat" size="30" id="<?php echo $this->get_field_id('adv_iframe'); ?>" name="<?php echo $this->get_field_name('adv_iframe'); ?>" > <?php echo $adv_iframe; ?> </textarea>
		</p>

	
		

		<?php		
	
	} //end of form
	
	function update($new_instance, $old_instance){

		$instance 						= $old_instance;
		$instance['startdate'] 			= $new_instance['startdate'];
		$instance['enddate'] 			= $new_instance['enddate'];	
		$instance['imagesec'] 			= $new_instance['imagesec'];
		$instance['imgurl'] 			= $new_instance['imgurl'];
		$instance['adv_iframe'] 		= $new_instance['adv_iframe'];		
		return $instance;
	}


		
}	
	
	function AdvertiseWidget_init() {
		register_widget('AdvertiseWidget');
	}	
	add_action('widgets_init', 'AdvertiseWidget_init');

}

