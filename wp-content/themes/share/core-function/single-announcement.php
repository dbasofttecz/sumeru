<?php get_header(); ?>
    <div class="header-banner">
        <div class="container">
            <div class="row">
                <div class="header-content-wrapper">
                    <h1><?php the_title(); ?></h1>
                    <div class="post-date-and-author">
                        Posted on: <time><?php the_date("Y-M-d"); ?></time>
                        Posted By: <?php the_author(); ?></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="single-content-wrapper">
                    <?php  the_content(); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
