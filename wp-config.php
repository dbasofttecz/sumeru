<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sumeru');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*?q34u8I$T8V@X7:S7iM^y7g{bEI_%>=h/.gAwAg}y2#>|C`kTHbV/Cg}<#<rPjh');
define('SECURE_AUTH_KEY',  'k1YtVPz7i|exHV0:juFZoLg&dVmhJ&4,tnorALRq`OsgD>IM,K51^3xx[5>TSYl-');
define('LOGGED_IN_KEY',    'w|4h^f`{0c(VcxusYP^?eI>5`i;Lj;n?#$PXW6+PVg+-z.LC0&lMemIsfpck9s~x');
define('NONCE_KEY',        '6A~_~x?52@U}2b4!,%Zb4G7Q7Lm@m4/Boe{v04{<160$ow[UCe!HAF%KefR@/ p7');
define('AUTH_SALT',        '3ce0^~#ER:n]:Z)]vAHstSfly/wee2nW]lV=x)ZCXNaAi41aqHf[n)(VB_X/FdfC');
define('SECURE_AUTH_SALT', '^ [C%3ZK:dOqX /0n;Tj4PT6QTZg A=BV#I~e4Se] fh+$5{Qj/z8*Xg8,-)y08S');
define('LOGGED_IN_SALT',   'iq>?,TpyK Lt69(iW}y:!$QT4bS_`ql,ZV1~jM^BTU4D Ns0k2Fd0s{wZs ApjjH');
define('NONCE_SALT',       '9$,yrVr`*+Mz-Hpy_PIN!YD~Ud)oJO^LRVyl:yI8c@cLo.[;*ga@,S~]FldKP<1L');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'test_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
